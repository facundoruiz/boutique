-- Cambio en denuncias datos NULL
ALTER TABLE `denuncias`
	CHANGE COLUMN `barrios_idbarrios` `barrios_idbarrios` INT(11) NULL DEFAULT NULL AFTER `ruta_medio`,
	CHANGE COLUMN `comisarias_idcomisarias` `comisarias_idcomisarias` INT(11) NULL DEFAULT NULL AFTER `barrios_idbarrios`;

-- comisarias
ALTER TABLE `comisarias`
	ADD COLUMN `zona` INT NOT NULL AFTER `nombre`,
	ADD COLUMN `direccion` VARCHAR(245) NULL AFTER `zona`;
-- ------------------------------------------
