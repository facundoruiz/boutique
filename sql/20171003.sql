-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.9-MariaDB-log - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para andhes
CREATE DATABASE IF NOT EXISTS `andhes` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `andhes`;

-- Volcando estructura para tabla andhes.barrios
CREATE TABLE IF NOT EXISTS `barrios` (
  `idbarrios` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(245) DEFAULT NULL,
  `users_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`idbarrios`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `fk_barrios_users1_idx` (`users_id`),
  CONSTRAINT `fk_barrios_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla andhes.barrios: ~14 rows (aproximadamente)
DELETE FROM `barrios`;
/*!40000 ALTER TABLE `barrios` DISABLE KEYS */;
INSERT INTO `barrios` (`idbarrios`, `nombre`, `users_id`) VALUES
	(2, 'SEOC', 1),
	(3, 'Villa Lujan', 1),
	(4, '24 deMarzo', 1),
	(5, '9 de julio', 1),
	(6, '17 de agosto', 1),
	(7, 'Oeste 2', 1),
	(8, 'congreso', 1),
	(9, '11 de marzo', 1),
	(10, 'mercofrut', 1),
	(11, 'policial 1', 1),
	(12, 'policial e2', 1),
	(13, 'Independencia', 1),
	(14, 'Ejercito Argentino', 1),
	(17, 'San Cayetano', 2);
/*!40000 ALTER TABLE `barrios` ENABLE KEYS */;

-- Volcando estructura para tabla andhes.comisarias
CREATE TABLE IF NOT EXISTS `comisarias` (
  `idcomisarias` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(245) DEFAULT NULL,
  `users_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`idcomisarias`),
  UNIQUE KEY `nombre` (`nombre`),
  KEY `fk_comisarias_users1_idx` (`users_id`),
  CONSTRAINT `fk_comisarias_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla andhes.comisarias: ~3 rows (aproximadamente)
DELETE FROM `comisarias`;
/*!40000 ALTER TABLE `comisarias` DISABLE KEYS */;
INSERT INTO `comisarias` (`idcomisarias`, `nombre`, `users_id`) VALUES
	(1, 'alderetes', 2),
	(3, 'seccional 3da', 2),
	(4, 'seccional 2da', 2);
/*!40000 ALTER TABLE `comisarias` ENABLE KEYS */;

-- Volcando estructura para tabla andhes.denuncias
CREATE TABLE IF NOT EXISTS `denuncias` (
  `iddenuncias` int(11) NOT NULL AUTO_INCREMENT,
  `sexo` varchar(1) DEFAULT NULL COMMENT 'F Femenino\nM Masculino',
  `edad` char(1) DEFAULT NULL,
  `residencia` varchar(100) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `barrio_comisaria` tinyint(1) DEFAULT NULL,
  `detalle` text,
  `detenido` char(1) DEFAULT NULL COMMENT 's SI\nn NO',
  `tiempo_detencion` int(11) DEFAULT NULL,
  `pago` char(1) DEFAULT NULL COMMENT 's SI\nn NO',
  `monto` varchar(45) DEFAULT NULL,
  `agresor_conoce` tinyint(1) DEFAULT NULL,
  `agresor_nombre` varchar(45) DEFAULT NULL,
  `agresor_apodo` varchar(45) DEFAULT NULL,
  `agresor_descripcion` text,
  `tipo_medio` char(1) DEFAULT NULL COMMENT 'v video\nf foto',
  `ruta_medio` varchar(245) DEFAULT NULL,
  `barrios_idbarrios` int(11) NOT NULL,
  `comisarias_idcomisarias` int(11) NOT NULL,
  PRIMARY KEY (`iddenuncias`),
  KEY `fk_denuncias_barrios1_idx` (`barrios_idbarrios`),
  KEY `fk_denuncias_comisarias1_idx` (`comisarias_idcomisarias`),
  CONSTRAINT `fk_denuncias_barrios1` FOREIGN KEY (`barrios_idbarrios`) REFERENCES `barrios` (`idbarrios`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_denuncias_comisarias1` FOREIGN KEY (`comisarias_idcomisarias`) REFERENCES `comisarias` (`idcomisarias`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla andhes.denuncias: ~0 rows (aproximadamente)
DELETE FROM `denuncias`;
/*!40000 ALTER TABLE `denuncias` DISABLE KEYS */;
/*!40000 ALTER TABLE `denuncias` ENABLE KEYS */;

-- Volcando estructura para tabla andhes.groups
CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla andhes.groups: ~2 rows (aproximadamente)
DELETE FROM `groups`;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` (`id`, `name`, `description`) VALUES
	(1, 'admin', 'Administrator'),
	(2, 'members', 'General User');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;

-- Volcando estructura para tabla andhes.login_attempts
CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla andhes.login_attempts: ~0 rows (aproximadamente)
DELETE FROM `login_attempts`;
/*!40000 ALTER TABLE `login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_attempts` ENABLE KEYS */;

-- Volcando estructura para tabla andhes.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla andhes.users: ~0 rows (aproximadamente)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
	(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1506920157, 1, 'Admin', 'istrator', 'ADMIN', '0'),
	(2, '127.0.0.1', 'fa@andhes.com', '$2y$08$L0Q.vD517v1LJxkA2MfJLeMLdcYVYWy.Vdw3mPtqpLm3NLFCloVyW', NULL, 'fa@andhes.com', NULL, 'NHOcjjYvntP0o22jVUyUa.a6a52feb06e045102f', 1506562479, NULL, 1506560404, 1506918488, 1, 'Facundo', 'Ruiz', 'Telecom', '38144444');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Volcando estructura para tabla andhes.users_groups
CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla andhes.users_groups: ~2 rows (aproximadamente)
DELETE FROM `users_groups`;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
	(1, 1, 1),
	(2, 1, 2),
	(3, 2, 2);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
