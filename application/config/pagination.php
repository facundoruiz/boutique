<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| Pagination Settings
| -------------------------------------------------------------------
| Reference: http://www.codeigniter.com/user_guide/libraries/pagination.html
| */

// Customizing the Pagination
/*$config['uri_segment']					= 3;				// default: 3
$config['num_links']					= 2;				// default: 2
$config['prefix']						= '';				// default: ''
$config['suffix']						= '';				// default: ''
$config['use_global_url_suffix']		= FALSE;			// default: FALSE
*/
// Customizing query string format
//$config['use_page_numbers']				= TRUE;				// default: TRUE
$config['page_query_string']			= TRUE;				// default: TRUE
$config['enable_query_strings']			= TRUE;				// default: FALSE
$config['query_string_segment']			= 'p';				// default: 'per_page'
$config['reuse_query_string']			= TRUE;				// default: FALSE

/** This Application Must Be Used With BootStrap 4 *  */
$config['full_tag_open'] 	= '<div class="pagging text-center"><nav><ul class="pagination">';
$config['full_tag_close'] 	= '</ul></nav></div>';
$config['num_tag_open'] 	= '<li class="page-item"><span class="page-link">';
$config['num_tag_close'] 	= '</span></li>';
$config['cur_tag_open'] 	= '<li class="page-item active"><span class="page-link">';
$config['cur_tag_close'] 	= '<span class="sr-only">(current)</span></span></li>';
$config['next_tag_open'] 	= '<li class="page-item"><span class="page-link">';
$config['next_tagl_close'] 	= '<span aria-hidden="true">&raquo;</span></span></li>';
$config['prev_tag_open'] 	= '<li class="page-item"><span class="page-link">';
$config['prev_tagl_close'] 	= '</span></li>';
$config['first_tag_open'] 	= '<li class="page-item"><span class="page-link">';
$config['first_tagl_close'] = '</span></li>';
$config['last_tag_open'] 	= '<li class="page-item"><span class="page-link">';
$config['last_tagl_close'] 	= '</span></li>';

// Hiding the Pages
$config['display_pages']				= TRUE;				// default: FALSE

// Adding attributes to anchors
$config['attributes']					= array('class'=>' page-link');			// default: array()
//$config['anchor_class']       = 'page-link';
// Disabling the "rel" attribute
$config['attributes']['rel']			= FALSE;			// default: FALSE
