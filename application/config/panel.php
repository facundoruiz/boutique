<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Panel config
| -------------------------------------------------------------------------
| ARchivo con configuracion inicial del panel backend
*/

$config['panel'] = [

	// Site name
	'name' => 'Backend',
	// Default page title
	'page_title' => 'Backend',
	// Default meta data (name => content)
	'meta'	=> [
		'author'		=> 'Facundo Ruiz',
		'description'	=> 'Panel de control'
	],
	'theme'=> 'theme-black',
	// Default scripts head / foot
	'scripts' => [
		'head'	=> ['plugins/jquery/jquery.min.js',	
						'plugins/bootstrap/js/bootstrap.js',
								
								],
		'foot'	=> [	'plugins/bootstrap-select/js/bootstrap-select.js',
						'plugins/jquery-slimscroll/jquery.slimscroll.js',
						'plugins/node-waves/waves.js',
						'plugins/autosize/autosize.js',
						'plugins/momentjs/moment.js',
						
						'js/admin.js',
						'js/demo.js'],
	],

	// Default stylesheets
	'stylesheets' => [
		'screen' => [				'https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext',
		'https://fonts.googleapis.com/icon?family=Material+Icons',
									'plugins/bootstrap/css/bootstrap.css',
									'plugins/font-awesome/css/font-awesome.min.css',
									'plugins/node-waves/waves.css',
									'plugins/animate-css/animate.css',
									'plugins/waitme/waitMe.css',
									'plugins/bootstrap-select/css/bootstrap-select.css',
									'css/style.css',
									'css/themes/theme-black.min.css']
	],

	// Menu items
	'menu' => [ // menu principal
				'welcome' => [
					'name'		=> 'Inicio',
					'url'		=> 'welcome',
					'icon' => 'home'
						],
				'users' => [
					'name'		=> 'Usuarios',
					'url'		=> 'users',
					'icon' => 'group'
					
						],
		]
];
