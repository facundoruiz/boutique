<div class="row movil">
      <div class="col-6">
         <img  class="logo " src="<?php echo base_url('assets/images/logo-espalda.png')?>" alt="<?=$name_back?>">
      </div>
      <div class="col-6">
        <a class="btn btn-primary home" data-toggle="modal" data-target="#exampleModal">
          <i class="fa fa-fw fa-sign-in"></i>Login</a>
      </div>
</div>



     <!-- cerrar sesión Modal-->
         <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
           <div class="modal-dialog" role="document">
             <div class="modal-content">
               <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel"><?php echo lang('login_heading');?></h5>
                 <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">×</span>
                 </button>
               </div>
               <div class="modal-body">

                   <?php echo lang('login_subheading');?>
             <?php echo form_open("auth/login");?>

               <p>
                 <?php echo lang('login_identity_label', 'identity');?>
                 <?php echo form_input($identity);?>
               </p>

               <p>
                 <?php echo lang('login_password_label', 'password');?>
                 <?php echo form_input($password);?>
               </p>

               <p>
                 <?php echo lang('login_remember_label', 'remember');?>
                 <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
               </p>


               <p><?php echo form_submit('submit', lang('login_submit_btn'),array('class' =>'btn btn-primary btn-block' ));?></p>

             <?php echo form_close();?>

             <p><a href="auth/forgot_password"  class="d-block small"><?php echo lang('login_forgot_password');?></a></p>

               <div class="modal-footer">
                 <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                 </div>
             </div>
           </div>
         </div>
        </div>
