

	<h1>Bienvenido!</h1>

	<div id="body">
		<div class="col-xl-3 col-sm-6 mb-3">
	          <div class="card text-black o-hidden h-100">
	            <div class="card-body">
	              <div class="card-body-icon">
	                <i class="fa fa-fw fa-file-text-o"></i>
	              </div>
	              <div class="mr-5"><?=$cant_denuncia?> Denuncias</div>
	            </div>
	            <a class="card-footer text-black clearfix small z-1" href="<?php echo site_url('denuncias') ?>">
	              <span class="float-left">ir</span>
	              <span class="float-right">
	                <i class="fa fa-angle-right"></i>
	              </span>
	            </a>
	          </div>
	        </div>	</div>
