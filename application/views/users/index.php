<h1><?php echo lang('index_heading');?></h1>
<p><?php echo lang('index_subheading');?></p>

<p><?php echo anchor('users/create_user', lang('index_create_user_link'),['class'=>'btn btn-primary','role'=>"button"])?></p>

<table class="table table-bordered table-striped table-hover table-responsive" id="dataTable" width="100%" cellspacing="0">
	  <thead  class="thead-inverse"><tr>
		<th><?php echo lang('index_fname_th');?></th>
		<th><?php echo lang('index_lname_th');?></th>
		<th><?php echo lang('index_email_th');?></th>

		<th><?php echo lang('index_status_th');?></th>
		<th><?php echo lang('index_action_th');?></th>
	</tr></thead>
	<tbody>
	<?php foreach ($users as $user):?>
		<tr>
            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
			<td><?php echo ($user->active) ? anchor("users/deactivate/".$user->id, lang('index_active_link'),['class'=>'badge badge-success','role'=>"button"]) : anchor("users/activate/". $user->id, lang('index_inactive_link'),['class'=>'badge badge-light','role'=>"button"]);?></td>
			<td><?php echo anchor("users/edit_user/".$user->id, '<i class="fa fa-pencil-square-o" aria-hidden="true" ></i>',['data-toggle'=>"tooltip",
																																																															'data-original-title'=>"Modificar",
																																																															'title'=>'Modificar',
																																																															'class'=>'btn  ',
																																																															'role'=>"button"]) ;?></td>
		</tr>
	<?php endforeach;?>
</tbody>
</table>
