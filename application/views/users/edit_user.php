    <div class="row clearfix">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="header">
           <div class="block-header">
            <h2><?php echo lang('edit_user_heading');?></h2>
          </div>
          <?php echo lang('edit_user_subheading');?>

          <div id="infoMessage"><?php echo $message;?></div>
        </div>

        <div class="body">    
          <?php echo form_open(uri_string());?>

          <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
              <div class="form-group form-float">
                <div class="form-line">
                  <?php //echo lang('edit_user_fname_label', 'first_name');?> <br />
                  <?php echo form_input($first_name);?>
                </div>
              </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
             <div class="form-group form-float">
              <div class="form-line">
                <?php echo lang('edit_user_lname_label', 'last_name',array('class' => 'form-label' ));?> <br />
                <?php echo form_input($last_name);?>
              </div>
            </div>
          </div>
        </div>
        <div class="row clearfix">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
            <div class="form-group form-float">
              <div class="form-line">
                <?php echo lang('edit_user_company_label', 'company');?> <br />
                <?php echo form_input($company);?>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
            <div class="form-group form-float">
              <div class="form-line">
                <?php echo lang('edit_user_phone_label', 'phone');?> <br />
                <?php echo form_input($phone);?>
              </div>
            </div>
          </div>
        </div>

        <div class="row clearfix">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
            <div class="form-group form-float">
              <div class="form-line">
                <?php echo lang('edit_user_password_label', 'password');?> <br />
                <?php echo form_input($password);?>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
            <div class="form-group form-float">
              <div class="form-line">
                <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?><br />
                <?php echo form_input($password_confirm);?>
              </div>
            </div>
          </div>
        </div>

  <div class="row clearfix">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
          <?php if ($this->ion_auth->is_admin()): ?>

            <h3><?php echo lang('edit_user_groups_heading');?></h3>
            <?php foreach ($groups as $group):?>
                <?php
                $gID=$group['id'];
                $checked = null;
                $item = null;
                foreach($currentGroups as $grp) {
                  if ($gID == $grp->id) {
                    $checked= ' checked="checked"';
                    break;
                  }
                }
                ?>
                <input type="checkbox" name="groups[]" id="cgroup<?php echo $group['id'];?>" value="<?php echo $group['id'];?>"<?php echo $checked;?> class="filled-in">
              <label class="checkbox" for="cgroup<?php echo $group['id'];?>">
                <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
              </label>
               
            <?php endforeach?>

          <?php endif ?>

          <?php echo form_hidden('id', $user->id);?>
          <?php echo form_hidden($csrf); ?>
                </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                        <?php echo form_submit('submit', lang('edit_user_submit_btn'));?>
                </div>
    </div>          
        <?php echo form_close();?>
      </div>
    </div>
  </div>
</div> 