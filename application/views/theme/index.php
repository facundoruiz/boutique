<?php $this->load->view('theme/header'); ?>

<body <?php if ( !empty($theme) ) echo " class='$theme'"; ?>>
 <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Cargando.....</p>
        </div>
    </div>
 <!--   #END# Page Loader -->
    <!-- MENU -->
    <?php $this->load->view('theme/menu'); ?>

    <section class="content">
        <div class="container-fluid">
         <!-- Breadcrumbs -->
         <?php echo $this->breadcrumbs->show();?>

         <!-- notice / messages -->
         <?php if (isset($notice)) : ?>
            <?php echo $notice; ?>
        <?php else : ?>
            <div id="notices"></div>
        <?php endif; ?>


        <?= $view_content ?>

    </div>
</section>
<!-- /.container-fluid-->
<!-- /.content-wrapper-->
<?php $this->load->view('theme/footer'); ?>
