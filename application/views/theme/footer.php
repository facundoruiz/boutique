
  
   <!-- Scroll to Top Button-->
   <a class="scroll-to-top rounded" href="#page-top">
     <i class="fa fa-angle-up"></i>
   </a>
   <!-- cerrar sesión Modal-->
       <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
           <div class="modal-content">
             <div class="modal-header">
               <h5 class="modal-title" id="exampleModalLabel">¿Listo para salir?</h5>
               <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">×</span>
               </button>
             </div>
             <div class="modal-body">Seleccione "Cerrar sesión" si está listo para finalizar su sesión actual.</div>
             <div class="modal-footer">
               <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
               <a class="btn btn-primary" href="<?php echo base_url('auth/logout')?>">cerrar sesión</a>
             </div>
           </div>
         </div>
       </div>
<?php

if (isset($external_scripts['foot']))
{
                   foreach ($external_scripts['foot'] as $script)
                   {
                   $url = strpos($script, 'http') ? $script : base_url('assets/'.$script);
                       echo "<script src='{$url}'></script>\n";
                   }
}
          ?>
</body>
</html>
