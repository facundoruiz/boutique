<?php $this->load->view('theme/header'); ?>
<body class=" bg-dark" id="page-top">

  <div class="container">
    <div class="row">
            <div class="col-md-12">

      <?php if (isset($notice)) : ?>
          <?php echo $notice; ?>
      <?php else : ?>
          <div id="notices"></div>
      <?php endif; ?>
      </div>
    </div>
            <?= $view_content ?>


<?php $this->load->view('theme/footer'); ?>
