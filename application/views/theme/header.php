<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
       <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">
	<meta name="description" content="">
  <meta name="author" content="">
  <title><?=$page_title;?></title>

<?php

if (isset($stylesheets))
{
    foreach ($stylesheets as $media => $files)
    {
      foreach ($files as $style ){
        //$url = strpos($style, 'https') ? $style : base_url('assets/'.$style);
        $url = (strpos($style, 'http')=== 0 || strpos($style, '//')=== 0) ? $style : base_url('assets/'.$style);
        echo "<link rel='stylesheet' type='text/css' href='{$url}' media='{$media}'/>\n";
      }
    }
}

   if (isset($external_scripts['head']))
  {
                      foreach ($external_scripts['head'] as $script)
                      {
                      //$url = strpos($script, "https")!==false ? $script : base_url('assets/'.$script);
                $url = (strpos($script, 'http')=== 0 || strpos($script, '//')=== 0) ? $script : base_url('assets/'.$script);
    
                          echo "<script src='{$url}'></script>\n";
                      }
  }

    ?>
     </head>
